<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form:errors path="*" cssClass="alert alert-warning" element="div" />
<form:hidden path="id"/>


<div class="row">
    <div class="col-lg-12">
        <label for="shopId">Идентификатор магазина(sMrchLogin)</label>
        <form:input path="shopId" cssClass="form-control" id="shopId"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="pass1">Пароль #1(sMrchPass1)</label>
        <form:input path="pass1" cssClass="form-control" id="pass1"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="pass2">Пароль #2</label>
        <form:input path="pass2" cssClass="form-control" id="pass2"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="url">URL для запроса</label>
        <form:input path="url" cssClass="form-control" id="url"/>
        <p>https://auth.robokassa.ru/Merchant/Index.aspx & http://test.robokassa.ru/Index.aspx</p>
    </div>
</div>


