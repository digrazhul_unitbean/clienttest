<%@ page import="com.ub.core.base.routes.BaseRoutes" %>
<%@ page import="com.ub.clientTest.client.routes.ClientRoutes" %>
<%@ page import="com.ub.clientTest.catalog.routes.CatalogRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%--
  Created by IntelliJ IDEA.
  User: digrazhul
  Date: 05.05.2015
  Time: 17:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8"/>
  <title>Добро дожаловать!</title>
  <link rel="stylesheet" href="/static/client/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="/static/client/css/custom.css"/>
</head>
<body>
  <div class="container">
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<%= ClientRoutes.ROOT%>" class="navbar-brand">ЮнитБин</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li>
              <a href="<%= ClientRoutes.ROOT%>">Главная</a>
            </li>
            <li>
              <a href="<%= BaseRoutes.ADMIN%>">Админ. панель</a>
            </li>
            <li>
              <a href="<%= ClientRoutes.BLOG_ALL%>">Блог</a>
            </li>
            <li>
              <a href="<%= CatalogRoutes.ALL%>">Каталог</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
      <tiles:insertAttribute name="content"/>
</body>
</html>
