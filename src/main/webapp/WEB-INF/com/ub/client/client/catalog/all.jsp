<%@ page import="com.ub.clientTest.catalog.routes.CatalogRoutes" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Каталог</title>
</head>
<body>


<div class="top-container container">
    <div class="row">
        <div class="col-md-4">
            <div class="btn-group-vertical" role="group" aria-label="Vertical button group">
            <c:forEach items="${childs.keySet()}" var="parent">

                <a href="#" class="btn btn-default">${parent.title}</a>

                <%-- <c:if test="${}"--%>
                <c:forEach items="${childs.get(parent)}" var="childs">
                <a href="#" class="btn btn-default">${childs.title}</a>

                </c:forEach>

            </c:forEach>
        </div>
    </div>
</div>
</div>
</body>
</html>
