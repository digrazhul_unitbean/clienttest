<%--
  Created by IntelliJ IDEA.
  User: digrazhul
  Date: 08.05.2015
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container top-container">

    <div class="row">
        <div class="col-md-12">
            <h1>${blogDoc.title}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>${blogDoc.content}</p>
        </div>
    </div>

</div>
