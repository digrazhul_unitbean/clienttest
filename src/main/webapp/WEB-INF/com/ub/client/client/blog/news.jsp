<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: digrazhul
  Date: 08.05.2015
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container top-container">

  <c:forEach items="${blogs}" var="blogDoc">
    <div class="row">
      <div class="col-md-12">
        <h3><a href="/blog/${blogDoc.url}">${blogDoc.title}</a> </h3>
        <p>${blogDoc.previewDescription}</p>
      </div>
    </div>
  </c:forEach>

</div>
