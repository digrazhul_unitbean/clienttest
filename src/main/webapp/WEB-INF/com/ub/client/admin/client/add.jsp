<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ub.clientTest.client.routes.ClientAdminRoutes" %>
<%--
  Created by IntelliJ IDEA.
  User: digrazhul
  Date: 06.05.2015
  Time: 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-md-12"><h1>Добавить</h1></div>
</div>
<div class="col-md-12">
    <form:form action="<%= ClientAdminRoutes.ADD%>" method="post" modelAttribute="clientDoc"
               enctype="multipart/form-data">

        <jsp:include page="form.jsp"/>

        <br>
        <button class="btn btn-success" type="submit">Сохранить</button>

    </form:form>
</div>
</body>
</html>
