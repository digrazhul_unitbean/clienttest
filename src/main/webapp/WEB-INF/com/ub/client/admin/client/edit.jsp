<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ub.clientTest.client.routes.ClientAdminRoutes" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title></title>
</head>
<body>
<div class="row">
  <div class="col-md-12"><h1>Редактирование</h1></div>
</div>
<div class="col-md-12">
  <form:form action="<%= ClientAdminRoutes.EDIT%>" method="post" modelAttribute="clientDoc"
             enctype="multipart/form-data">

    <%--<c:set var="clientDoc" value="${clientDoc}" scope="request"/>--%>
    <jsp:include page="form.jsp"/>

    <br>
    <button class="btn btn-success" type="submit">Сохранить</button>

  </form:form>
</div>
</body>
</html>