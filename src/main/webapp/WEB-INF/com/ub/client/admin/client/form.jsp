<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: digrazhul
  Date: 06.05.2015
  Time: 15:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>

<c:if test="${not empty clientDoc.id}">
    <form:hidden path="id"/>
</c:if>

<div class="row">
    <div class="col-md-12">
        <label for="email">E-mail</label>
        <form:input path="email" cssClass="form-control" id="email"/>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label for="name">Имя</label>
        <form:input path="name" cssClass="form-control" id="name"/>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label for="lastName">Фамилия</label>
        <form:input path="lastName" cssClass="form-control" id="lastName"/>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label for="picFile">Аватар</label>
        <c:if test="${not empty clientDoc.pic}">
            <form:hidden path="pic" cssClass="form-control" id="pic"/>
        </c:if>
        <input name="picFile" type="file" id="picFile"/>
        <c:if test="${not empty clientDoc.pic}">
            <img src="/pics/${clientDoc.pic}"/>
        </c:if>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label for="fileFile">Карта партнера (<a href="/files/${clientDoc.file}">текущая карта</a>)</label>
        <c:if test="${not empty clientDoc.file}">
            <form:hidden path="file" cssClass="form-control" id="file"/>
        </c:if>
        <input name="fileFile" type="file" id="fileFile"/>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label for="typeUser">Тип пользователя</label>
        <form:select path="clientTypeDoc.id" cssClass="form-control" id="typeUser">
            <c:forEach items="${clientTypes}" var="cType">
                <form:option value="${cType.id}">${cType.title}</form:option>
            </c:forEach>
        </form:select>
    </div>
</div>
</body>
</html>
