<%@ page import="com.ub.shop.item.routes.ItemAdminRoutes" %>
<%@ page import="com.ub.shop.item.models.ItemDoc" %>
<%@ page import="com.ub.shop.catalog.routes.CatalogAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="tab-pane " id="seo-tags">
    <!-- Информация о товаре-->
    <form:form action="<%= ItemAdminRoutes.EDIT_SEO%>" modelAttribute="seoTags" method="POST"
               enctype="multipart/form-data">
        <form:errors path="*" cssClass="alert alert-warning" element="div"/>
        <input type="hidden" name="id" value="${itemDoc.id}"/>
        <jsp:include page="/WEB-INF/com/ub/core/admin/seoTags/form.jsp"/>
        <br>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" id="btn-item-seo-save">
                Сохранить
            </button>
        </div>
    </form:form>
</div>


<script>
    $(function () {
        $('#btn-item-seo-save').click(function () {
            $.post('<%= ItemAdminRoutes.EDIT_SEO%>',
                    {
                        id: '${itemDoc.id}',
                        metaKeywords: $('#seoTags-metaKeywords').val(),
                        metaDescription: $('#seoTags-metaDescription').val(),
                        metaTitle: $('#seoTags-metaTitle').val(),
                        title: $('#seoTags-title').val(),
                        h1: $('#seoTags-h1').val()
                    }, function (data) {
                        var opts = {
                            "closeButton": true,
                            "debug": false,
                            "positionClass": "toast-top-right",
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.success("Данные о товаре изменены.", "Запрос выполнен", opts);
                    }
            );
            return false;
        });
    });
</script>
