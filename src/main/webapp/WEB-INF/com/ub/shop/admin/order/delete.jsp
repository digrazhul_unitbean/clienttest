<%@ page import="com.ub.shop.order.routes.OrderAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="row">
     <div class="col-md-6">
         <ol class="breadcrumb bc-3">
             <li>
                 <a href="<c:url value="/admin"/>"><i class="entypo-home"></i>Главная</a>
             </li>
             <li>
                 <a href="<c:url value="<%= OrderAdminRoutes.ALL%>"/>">Все Заказы</a>
             </li>
             <li class="active">
                 <strong>Удаление</strong>
             </li>
         </ol>
     </div>
     <div class="col-md-6 text-right">
        <c:url value="<%= OrderAdminRoutes.EDIT%>" var="editUrl">
            <c:param name="id" value="${id}"/>
        </c:url>
        <a href="<c:url value="${editUrl}"/>" class="btn btn-default">Вернуться к редактированию</a>
     </div>
 </div>
<div class="row">
    <div class="col-lg-12">
        <div class="widget widget-green" id="widget_profit_chart">
            <div class="widget-title">
                <h3><i class="icon-tasks"></i> Внимание, Вы собираетесь удалить документ! </h3>
            </div>
            <div class="widget-content">

                <form action="<%= OrderAdminRoutes.DELETE%>" method="POST">
                    <input type="hidden" value="${id}" name="id"/>
                    <div class="row">
                        <div class="col-lg-12">
                          <h2>Подтвердите удаление!</h2>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <button type="submit" id="contact-form-settings-submit" class="btn btn-primary">Подтвердить</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>