<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form:errors path="*" cssClass="alert alert-warning" element="div" />
<form:hidden path="id"/>


<div class="row">
    <div class="col-lg-12">
        <label for="city">Город</label>
        <form:input path="city" cssClass="form-control" id="city"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="street">Улица</label>
        <form:input path="street" cssClass="form-control" id="street"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="house">Номер дома</label>
        <form:input path="house" cssClass="form-control" id="house"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="apartment">Номер квартиры/офиса</label>
        <form:input path="apartment" cssClass="form-control" id="apartment"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="postcode">Почтовый индекс</label>
        <form:input path="postcode" cssClass="form-control" id="postcode"/>
    </div>
</div>

