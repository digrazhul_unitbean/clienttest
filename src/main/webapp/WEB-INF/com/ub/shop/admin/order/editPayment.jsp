<%@ page import="com.ub.shop.order.routes.OrderAdminRoutes" %>
<%@ page import="com.ub.shop.order.models.payment.PaymentType" %>
<%@ page import="com.ub.shop.order.models.payment.PaymentStatus" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<section id="section-delivery">
    <div class="row">
        <div class="col-lg-6">
            <label for="payment-type">Тип оплаты</label>
            <select name="payment-type" class="form-control" id="payment-type"/>
            <c:forEach items="<%= com.ub.shop.order.models.payment.PaymentType.types%>" var="type">
                <option value="${type.id}" <c:if test="${type.id == orderDoc.payment.type.id}">selected="selected"</c:if> >
                        ${type.title}
                </option>
            </c:forEach>
            </select>
        </div>
        <div class="col-lg-6">
            <label for="payment-status">Статус оплаты</label>
            <select name="payment-status" class="form-control" id="payment-status"/>
            <c:forEach items="<%= com.ub.shop.order.models.payment.PaymentStatus.statuses%>" var="status">
                <option value="${status.id}" <c:if test="${status.id == orderDoc.payment.status.id}">selected="selected"</c:if> >
                        ${status.title}
                </option>
            </c:forEach>
            </select>
        </div>
    </div>
    <br>
    <hr>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <h3>Информация о платеже</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <span><b>Номер платежа</b> - ${orderDoc.payment.id}</span>
        </div>
        <div class="col-lg-4">
            <span><b>Дата оплаты</b> - <fmt:formatDate value="${orderDoc.payment.paymentDate}" pattern="yyyy-MM-dd HH:mm" /></span>
        </div>
        <div class="col-lg-4">
            <span><b>Итого к оплате</b> - ${orderDoc.payment.price}</span>
        </div>
    </div>

    <br>
    <hr>
    <br>

    <div class="form-group">
        <button type="submit" id="paument-save" class="btn btn-primary">
            Сохранить
        </button>
    </div>
</section>

<script>
    $('#paument-save').click(function () {
        $.post("<%= OrderAdminRoutes.EDIT_PAYMENT%>",
                {
                    "id": $('#order-id').val(),
                    "status": $('#payment-status option:selected').val(),
                    "type": $('#payment-type option:selected').val()
                },
                function (data) {
                    var opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Данные о оплате изменены", "Запрос выполнен", opts);
                });
    });
</script>