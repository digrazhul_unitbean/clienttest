<%@ page import="com.ub.shop.order.routes.OrderAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<style>
    .curcor-pointer:hover {
        cursor: pointer;
        background-color: whitesmoke;
    }
</style>
<div class="row" style="margin-top: 10px">
    <div class="col-lg-12">
        <table class="table table-bordered" id="table-1">
            <thead>
            <tr>
               <th>Данные о доставке</th><th>Оплата</th><th>Товары</th><th>Статус заказа</th><th>Данные пользователя</th><th>Заказ создан</th><th>Последнее обновление заказа</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${searchOrderAdminResponse.result}" var="doc">

                <tr class="curcor-pointer modal-order-line" data-id="${doc.id}" data-title="${doc.title}">
                   <td>${doc.delivery}</td><td>${doc.payment}</td><td>${doc.items}</td><td>${doc.status}</td><td>${doc.userInfo}</td><td>${doc.createDate}</td><td>${doc.lastUpdate}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        <ul class="pagination pagination-sm">

            <li>
                <a href="#" class="modal-order-goto" data-query="${searchOrderAdminResponse.query}"
                   data-number="${searchOrderAdminResponse.prevNum()}">
                <i class="entypo-left-open-mini"></i></a>
            </li>
            <c:forEach items="${searchOrderAdminResponse.paginator()}" var="page">
                <li class="<c:if test="${searchOrderAdminResponse.currentPage eq page}">active</c:if>">
                    <a href="#" class="modal-order-goto" data-query="${searchOrderAdminResponse.query}"
                       data-number="${page}">${page + 1}</a>
                </li>
            </c:forEach>
            <li>
                <a href="#" class="modal-order-goto" data-query="${searchOrderAdminResponse.query}"
                   data-number="${searchOrderAdminResponse.nextNum()}"><i class="entypo-right-open-mini"></i></a>
            </li>
        </ul>
    </div>
</div>