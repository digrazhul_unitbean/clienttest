<%@ page import="com.ub.shop.order.routes.OrderAdminRoutes" %>
<%@ page import="com.ub.shop.order.models.OrderStatus" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="row">
    <div class="col-md-6">
        <ol class="breadcrumb bc-3">
            <li>
                <a href="<c:url value="/admin"/>"><i class="entypo-home"></i>Главная</a>
            </li>
            <li class="active">
                <strong>Все Заказы</strong>
            </li>
        </ol>
    </div>
    <div class="col-md-6 text-right">
        <form action="<%= OrderAdminRoutes.ADD%>" method="POST">
            <button type="submit" class="btn btn-default">Добавить</button>
        </form>
    </div>
</div>

<div class="row">
    <form action="<%= OrderAdminRoutes.ALL%>" method="GET">
        <div class="col-lg-3">
                <input type="text" class="form-control input-sm" id="query" name="query"
                       value="${searchOrderAdminResponse.query}" placeholder="Поиск" style="width: 100%"/>

        </div>
        <div class="col-md-3">
            <select name="status" class="form-control input-sm" id="order-form-status"/>
            <c:forEach items="<%= OrderStatus.statuses%>" var="status">

                <option value="${status.id}" <c:if test="${status.id == searchOrderAdminResponse.status}">selected="selected"</c:if> >
                        ${status.title}
                </option>
            </c:forEach>
            <option value="" <c:if test="${searchOrderAdminResponse.status eq null || searchOrderAdminResponse.status eq ''}">selected="selected"</c:if> >Все статусы</option>
            </select>
        </div>
        <div class="col-md-3">
            <button type="submit" class="btn btn-sm btn-default"><i class="entypo-search">Поиск </i></button>
        </div>
    </form>
</div>

<div class="widget widget-blue">
    <div class="widget-title">
        <h3><i class="icon-table"></i> Результаты поиска: </h3>
    </div>
    <div class="widget-content">
        <div class="table-responsive">
            <table class="table table-bordered" id="table-1">
                <thead>
                <tr>
                    <th>Номер</th>
                    <th>Телефон</th>
                    <th>Email</th>
                    <th>Статус заказа</th>
                    <th>Заказ создан</th>
                    <th>Сумма</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${searchOrderAdminResponse.result}" var="doc">

                    <tr>
                        <td>${doc.payment.id}</td>
                        <td>${doc.userInfo.phone}</td>
                        <td>${doc.userInfo.email}</td>
                        <td>${doc.status.title}</td>
                        <td><fmt:formatDate value="${doc.createDate}" pattern="yyyy-MM-dd HH:mm" /></td>
                        <td>${doc.payment.price}</td>

                        <td class="">
                            <c:url value="<%= OrderAdminRoutes.EDIT%>" var="editUrl">
                                <c:param name="id" value="${doc.id}"/>
                            </c:url>
                            <a href="${editUrl}" class="btn btn-default btn-xs">
                                <i class="icon-pencil">Редактировать</i>
                            </a>
                            <c:url value="<%= OrderAdminRoutes.DELETE%>" var="urlDelete">
                                <c:param name="id" value="${doc.id}"></c:param>
                            </c:url>
                            <a href="${urlDelete}" type="submit" class="btn btn-xs btn-danger  ">
                                <i class="icon-remove">Удалить</i>
                            </a>

                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 text-center">
        <ul class="pagination pagination-sm">
            <c:url value="<%= OrderAdminRoutes.ALL%>" var="urlPrev">
                <c:param name="query" value="${searchOrderAdminResponse.query}"/>
                <c:param name="currentPage" value="${searchOrderAdminResponse.prevNum()}"/>
            </c:url>
            <li><a href="${urlPrev}"><i class="entypo-left-open-mini"></i></a></li>

            <c:forEach items="${searchOrderAdminResponse.paginator()}" var="page">
                <c:url value="<%= OrderAdminRoutes.ALL%>" var="urlPage">
                    <c:param name="query" value="${searchOrderAdminResponse.query}"/>
                    <c:param name="currentPage" value="${page}"/>
                </c:url>
                <li class="<c:if test="${searchOrderAdminResponse.currentPage eq page}">active</c:if>">
                    <a href="${urlPage}">${page + 1}</a>
                </li>
            </c:forEach>
            <c:url value="<%= OrderAdminRoutes.ALL%>" var="urlNext">
                <c:param name="query" value="${searchOrderAdminResponse.query}"/>
                <c:param name="currentPage" value="${searchOrderAdminResponse.nextNum()}"/>
            </c:url>
            <li><a href="${urlNext}"><i class="entypo-right-open-mini"></i></a></li>
        </ul>
    </div>
</div>
