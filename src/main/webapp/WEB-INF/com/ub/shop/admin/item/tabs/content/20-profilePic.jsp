<%@ page import="com.ub.shop.item.routes.ItemAdminRoutes" %>
<%@ page import="com.ub.shop.item.models.ItemDoc" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="tab-pane " id="profile-pic">

    <!-- Изображения для товара-->
    <form action="<%= ItemAdminRoutes.EDIT_IMAGE_ADD%>" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="${itemDoc.id}"/>

        <div class="row">

            <div class="col-lg-9">
                <input type="file" name="file" class="form-control" id="file"/>
            </div>
            <div class="col-lg-3 text-center">
                <button type="submit" class="btn btn-primary">
                    Добавить новое изображение
                </button>
            </div>
        </div>
    </form>
    <br>
    <hr>
    <br>

    <div class="row">
        <c:forEach items="${itemDoc.photos}" var="photo">
            <div class="col-lg-3" id="item-pic-${photo}">
                <img src="/pics/${photo}" style="width: 100%"/>
                <br>
                <c:if test="${itemDoc.mainPhoto eq photo}">
                                <span>
                                    <b>Основное изображение</b>
                                </span>
                </c:if>
                <c:if test="${itemDoc.mainPhoto ne photo}">
                    <a href="#" class="item-set-main-image" data-id="${itemDoc.id}"
                       data-pic-id="${photo}">
                        Сделать основным
                    </a>
                </c:if>
                <br>
                <a href="#" class="item-remove-image" data-id="${itemDoc.id}"
                   data-pic-id="${photo}">
                    Удалить
                </a>
            </div>
        </c:forEach>
    </div>

</div>
