<%@ page import="com.ub.shop.sale.routes.SaleAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
    .curcor-pointer:hover {
        cursor: pointer;
        background-color: whitesmoke;
    }
</style>
<div class="row" style="margin-top: 10px">
    <div class="col-lg-12">
        <table class="table table-bordered" id="table-1">
            <thead>
            <tr>
                <th>Заголовок</th>
                <th>Начало распродажи</th>
                <th>Окончание распродажи</th>
                <th>Процент скидки</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${searchSaleAdminResponse.result}" var="doc">

                <tr class="curcor-pointer modal-sale-line" data-id="${doc.id}" data-title="${doc.title}">
                    <td>${doc.title}</td>
                    <td><fmt:formatDate value="${doc.startDate}" pattern="yyyy-MM-dd HH:mm" /></td>
                    <td><fmt:formatDate value="${doc.endDate}" pattern="yyyy-MM-dd HH:mm" /></td>
                    <td>${doc.percent}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        <ul class="pagination pagination-sm">

            <li>
                <a href="#" class="modal-sale-goto" data-query="${searchSaleAdminResponse.query}"
                   data-number="${searchSaleAdminResponse.prevNum()}">
                    <i class="entypo-left-open-mini"></i></a>
            </li>
            <c:forEach items="${searchSaleAdminResponse.paginator()}" var="page">
                <li class="<c:if test="${searchSaleAdminResponse.currentPage eq page}">active</c:if>">
                    <a href="#" class="modal-sale-goto" data-query="${searchSaleAdminResponse.query}"
                       data-number="${page}">${page + 1}</a>
                </li>
            </c:forEach>
            <li>
                <a href="#" class="modal-sale-goto" data-query="${searchSaleAdminResponse.query}"
                   data-number="${searchSaleAdminResponse.nextNum()}"><i class="entypo-right-open-mini"></i></a>
            </li>
        </ul>
    </div>
</div>