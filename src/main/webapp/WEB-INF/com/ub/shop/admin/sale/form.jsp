<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form:errors path="*" cssClass="alert alert-warning" element="div" />
<form:hidden path="id"/>


<div class="row">
    <div class="col-lg-6">
        <label for="url">url</label>
        <form:input path="url" cssClass="form-control" id="url"/>
    </div>

    <div class="col-lg-6">
        <label for="title">Заголовок</label>
        <form:input path="title" cssClass="form-control" id="title"/>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-12">
        <label for="description">Описание</label>
        <form:textarea path="description" cssClass="form-control ckeditor" id="description" style="min-height:337px"/>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-4">
        <label for="startDate">Начало распродажи</label>
        <form:input path="startDate" cssClass="form-control" id="startDate"/>
    </div>

    <div class="col-lg-4">
        <label for="endDate">Окончание распродажи</label>
        <form:input path="endDate" cssClass="form-control" id="endDate"/>
    </div>

    <div class="col-lg-4">
        <label for="percent">Процент скидки</label>
        <form:input path="percent" cssClass="form-control" id="percent"/>

    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-12">
        <label for="mainPicFile">Изображение</label>
        <input type="file" name="mainPicFile" id="mainPicFile"/>
        <%--<form:hidden path="mainPic" cssClass="form-control" id="mainPic" />--%>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        Текущее изображение
        <br>
        <img src="/pics/${saleDoc.mainPic}" style="max-width: 100%"/>
    </div>
</div>

