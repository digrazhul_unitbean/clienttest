<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.pages.routes.PagesAdminRoutes" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ page import="com.ub.shop.order.routes.OrderAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="row">
    <div class="col-md-12">
        <strong>Информация о пользователе</strong>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <label for="userinfo-name">Фамилия</label>
        <input name="userinfo-name" class="form-control" id="userinfo-name"
               value="${orderDoc.userInfo.lastName}"/>
    </div>
    <div class="col-lg-3">
        <label for="userinfo-lastname">Имя</label>
        <input name="userinfo-lastname" class="form-control" id="userinfo-lastname"
               value="${orderDoc.userInfo.firstName}"/>
    </div>
    <div class="col-lg-3">
        <label for="userinfo-phone">Телефон</label>
        <input name="userinfo-phone" class="form-control" id="userinfo-phone"
               value="${orderDoc.userInfo.phone}"/>
    </div>
    <div class="col-lg-3">
        <label for="userinfo-email">Email</label>
        <input name="userinfo-email" class="form-control" id="userinfo-email"
               value="${orderDoc.userInfo.email}"/>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <button id="userinfo-save" class="btn btn-success">Сохранить</button>
    </div>
</div>

<script>
    $('#userinfo-save').click(function () {
        $.post("<%= OrderAdminRoutes.EDIT_USER_INFO%>",
                {
                    "id": $('#order-id').val(),
                    "name": $('#userinfo-name').val(),
                    "lastName": $('#userinfo-lastname').val(),
                    "email": $('#userinfo-email').val(),
                    "phone": $('#userinfo-phone').val()
                },
                function (data) {
                    var opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Информация о пользователе изменена", "Запрос выполнен", opts);
                });
    });
</script>