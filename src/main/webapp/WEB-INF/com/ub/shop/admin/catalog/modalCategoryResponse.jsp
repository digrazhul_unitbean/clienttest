<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.pages.routes.PagesAdminRoutes" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ page import="com.ub.shop.catalog.routes.CatalogAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<style>
    .curcor-pointer:hover {
        cursor: pointer;
        background-color: whitesmoke;
    }
</style>
<div class="row" style="margin-top: 10px">
    <div class="col-lg-12">
        <table class="table table-bordered" id="table-1">
            <thead>
            <tr>
                <th>Название</th>
                <th>Родительский раздел</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${searchCatalogAdminResponse.result}" var="doc">

                <tr class="curcor-pointer modal-catalog-line" data-id="${doc.id}" data-title="${doc.title}">
                    <td>${doc.title}</td>
                    <td>${doc.getParentDoc().title}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        <ul class="pagination pagination-sm">

            <li>
                <a href="#" class="modal-catalog-goto" data-query="${searchCatalogAdminResponse.query}"
                   data-number="${searchCatalogAdminResponse.prevNum()}">
                <i class="entypo-left-open-mini"></i></a>
            </li>
            <c:forEach items="${searchCatalogAdminResponse.paginator()}" var="page">
                <li class="<c:if test="${searchCatalogAdminResponse.currentPage eq page}">active</c:if>">
                    <a href="#" class="modal-catalog-goto" data-query="${searchCatalogAdminResponse.query}"
                       data-number="${page}">${page + 1}</a>
                </li>
            </c:forEach>
            <li>
                <a href="#" class="modal-catalog-goto" data-query="${searchCatalogAdminResponse.query}"
                   data-number="${searchCatalogAdminResponse.nextNum()}"><i class="entypo-right-open-mini"></i></a>
            </li>
        </ul>
    </div>
</div>
