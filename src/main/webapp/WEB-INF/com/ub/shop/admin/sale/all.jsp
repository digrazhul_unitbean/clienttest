<%@ page import="com.ub.shop.sale.routes.SaleAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="row">
    <div class="col-md-6">
        <ol class="breadcrumb bc-3">
            <li>
                <a href="<c:url value="/admin"/>"><i class="entypo-home"></i>Главная</a>
            </li>
            <li class="active">
                <strong>Все Распродажи(акции)</strong>
            </li>
        </ol>
    </div>
    <div class="col-md-6 text-right">
        <a href="<c:url value="<%= SaleAdminRoutes.ADD%>"/>" class="btn btn-default">Добавить</a>
    </div>
</div>

<div class="row">
    <form action="<%= SaleAdminRoutes.ALL%>" method="GET">
        <div class="col-lg-5">
            <div class="input-group">
                <input type="text" class="form-control input-sm" id="query" name="query"
                       value="${searchSaleAdminResponse.query}" placeholder="Поиск"/>

                <div class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-default"><i class="entypo-search">Поиск </i></button>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="widget widget-blue">
    <div class="widget-title">
        <h3><i class="icon-table"></i> Результаты поиска: </h3>
    </div>
    <div class="widget-content">
        <div class="table-responsive">
            <table class="table table-bordered" id="table-1">
                <thead>
                <tr>

                    <th>url</th>
                    <th>Заголовок</th>
                    <th>Начало распродажи</th>
                    <th>Окончание распродажи</th>
                    <th>Процент скидки</th>

                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${searchSaleAdminResponse.result}" var="doc">

                    <tr>
                        <td>${doc.url}</td>
                        <td>${doc.title}</td>
                        <td><fmt:formatDate value="${doc.startDate}" pattern="yyyy-MM-dd HH:mm" /></td>
                        <td><fmt:formatDate value="${doc.endDate}" pattern="yyyy-MM-dd HH:mm" /></td>
                        <td>${doc.percent}</td>

                        <td class="">
                            <c:url value="<%= SaleAdminRoutes.EDIT%>" var="editUrl">
                                <c:param name="id" value="${doc.id}"/>
                            </c:url>
                            <a href="${editUrl}" class="btn btn-default btn-xs">
                                <i class="icon-pencil">Редактировать</i>
                            </a>
                            <c:url value="<%= SaleAdminRoutes.DELETE%>" var="urlDelete">
                                <c:param name="id" value="${doc.id}"></c:param>
                            </c:url>
                            <a href="${urlDelete}" type="submit" class="btn btn-xs btn-danger  ">
                                <i class="icon-remove">Удалить</i>
                            </a>

                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 text-center">
        <ul class="pagination pagination-sm">
            <c:url value="<%= SaleAdminRoutes.ALL%>" var="urlPrev">
                <c:param name="query" value="${searchSaleAdminResponse.query}"/>
                <c:param name="currentPage" value="${searchSaleAdminResponse.prevNum()}"/>
            </c:url>
            <li><a href="${urlPrev}"><i class="entypo-left-open-mini"></i></a></li>

            <c:forEach items="${searchSaleAdminResponse.paginator()}" var="page">
                <c:url value="<%= SaleAdminRoutes.ALL%>" var="urlPage">
                    <c:param name="query" value="${searchSaleAdminResponse.query}"/>
                    <c:param name="currentPage" value="${page}"/>
                </c:url>
                <li class="<c:if test="${searchSaleAdminResponse.currentPage eq page}">active</c:if>">
                    <a href="${urlPage}">${page + 1}</a>
                </li>
            </c:forEach>
            <c:url value="<%= SaleAdminRoutes.ALL%>" var="urlNext">
                <c:param name="query" value="${searchSaleAdminResponse.query}"/>
                <c:param name="currentPage" value="${searchSaleAdminResponse.nextNum()}"/>
            </c:url>
            <li><a href="${urlNext}"><i class="entypo-right-open-mini"></i></a></li>
        </ul>
    </div>
</div>
