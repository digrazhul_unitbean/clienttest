<%@ page import="com.ub.shop.item.routes.ItemAdminRoutes" %>
<%@ page import="com.ub.shop.item.models.ItemDoc" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="tab-pane" id="profile-price">
    <!-- Цена товара. Склад. (цена - )-->
    <form:form action="<%= ItemAdminRoutes.EDIT%>" modelAttribute="itemDoc" method="POST"
               enctype="multipart/form-data">
        <form:errors path="*" cssClass="alert alert-warning" element="div"/>
        <form:hidden path="id" id="item-id"/>
        <hr/>
        <div class="row">
            <div class="col-lg-3">
                <label for="price">Цена</label>
                <form:input path="price" cssClass="form-control" id="price"/>
            </div>
            <div class="col-lg-3">
                <label for="balanceInStock">Остаток на складе</label>
                <form:input path="balanceInStock" cssClass="form-control" id="balanceInStock"/>
            </div>

            <div class="col-lg-3">
                <label for="takeIntoAccountTheBalanceInStock">Учитывать остаток на складе</label>
                <form:checkbox path="takeIntoAccountTheBalanceInStock" cssClass="form-control"
                               id="takeIntoAccountTheBalanceInStock"/>
            </div>
            <div class="col-lg-3">
                <label for="available">Товар доступен</label>
                <form:checkbox path="available" cssClass="form-control" id="available"/>
            </div>

        </div>
        <hr/>
        <div class="row">
            <div class="col-lg-6">
                <label for="parent-sale">Распродажа</label>
                <input type="hidden" id="parent-sale-hidden" name="saleId" class="form-control"
                       value="${itemDoc.saleId}" required="required"/>
                <input type="text" class="form-control" id="parent-sale"
                       value="${itemDoc.saleStamp.title}" readonly="readonly"/>
                <a href="#" id="btn_parent_sale" class="btn btn-blue"
                   style="margin-top: 10px">Выбрать</a>
                <a href="#" id="btn_parent_sale_clear" class="btn"
                   style="margin-top: 10px">Очистить</a>
            </div>
            <div class="col-lg-2">
                <label for="price">Цена со скидкой</label>
                <form:input path="discountPrice" cssClass="form-control" id="price"
                            readonly="true"/>
            </div>
            <div class="col-lg-2">
                <label for="price">Скидка действует с</label>
                <form:input path="discountPriceStartDate" cssClass="form-control" id="price"
                            readonly="true"/>
            </div>
            <div class="col-lg-2">
                <label for="price">Скидка действует по</label>
                <form:input path="discountPriceStopDate" cssClass="form-control" id="price"
                            readonly="true"/>
            </div>
        </div>
        <hr/>
        <br>

        <div class="form-group">
            <button type="submit" id="item-save-price" class="btn btn-primary">
                Сохранить
            </button>
        </div>
    </form:form>
</div>
