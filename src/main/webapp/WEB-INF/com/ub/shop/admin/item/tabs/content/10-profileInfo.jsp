<%@ page import="com.ub.shop.item.routes.ItemAdminRoutes" %>
<%@ page import="com.ub.shop.item.models.ItemDoc" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="tab-pane active" id="profile-info">
    <!-- Информация о товаре-->
    <form:form action="<%= ItemAdminRoutes.EDIT%>" modelAttribute="itemDoc" method="POST"
               enctype="multipart/form-data">
        <form:errors path="*" cssClass="alert alert-warning" element="div"/>
        <form:hidden path="id"/>
        <div class="row">
            <div class="col-lg-4">
                <label for="article">Артикул</label>
                <form:input path="article" cssClass="form-control" id="article"/>
            </div>

            <div class="col-lg-4">
                <label for="url">url</label>
                <form:input path="url" cssClass="form-control" id="url"/>
            </div>
            <div class="col-lg-4">
                <label for="title">Заголовок</label>
                <form:input path="title" cssClass="form-control" id="title"/>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-lg-12">
                <label for="description">Описание</label>
                <form:textarea path="description" cssClass="form-control ckeditor"
                               id="description"/>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-lg-3">
                <label for="isTop">Top товар</label>
                <form:checkbox path="isTop" cssClass="form-control" id="isTop"/>
            </div>

            <div class="col-lg-3">
                <label for="isNew">Новый товар</label>
                <form:checkbox path="isNew" cssClass="form-control" id="isNew"/>
            </div>
        </div>
        <hr/>

        <div class="row">
            <div class="col-lg-6">
                <label for="parent">Каталог</label>
                <input type="hidden" id="parent-hidden" name="catalogId" class="form-control"
                       value="${itemDoc.catalogId}" required="required"/>
                <input type="text" class="form-control" id="parent"
                       value="${itemDoc.catalogStamp.title}" readonly="readonly"/>
                <a href="#" id="btn_parent_catalog" class="btn btn-blue"
                   style="margin-top: 10px">Выбрать</a>
                <a href="#" id="btn_parent_catalog_clear" class="btn" style="margin-top: 10px">Очистить</a>
            </div>
            <div class="col-lg-6">
                <label for="parent-brand">Бренд</label>
                <input type="hidden" id="parent-brand-hidden" name="brandId" class="form-control"
                       value="${itemDoc.brandId}" required="required"/>
                <input type="text" class="form-control" id="parent-brand"
                       value="${itemDoc.brandStamp.title}" readonly="readonly"/>
                <a href="#" id="btn_parent_brand" class="btn btn-blue" style="margin-top: 10px">Выбрать</a>
                <a href="#" id="btn_parent_brand_clear" class="btn" style="margin-top: 10px">Очистить</a>
            </div>
        </div>


        <br>

        <div class="form-group">
            <button type="submit" id="item-save-info" class="btn btn-primary">Сохранить
            </button>
        </div>
    </form:form>
</div>
