<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.pages.routes.PagesAdminRoutes" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ page import="com.ub.shop.item.routes.ItemAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script>
    function removePic() {
        var vId = $(this).attr('data-id');
        var vPicId = $(this).attr('data-pic-id');

        $.post("<%= ItemAdminRoutes.EDIT_IMAGE_DELETE%>",
                {id: vId, picId: vPicId},
                function (data) {
                    var opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Изображение удалено.", "Запрос выполнен", opts);
                    $('#item-pic-' + vPicId).remove();
                });
        return false;
    }
    function setMainPic() {
        var vId = $(this).attr('data-id');
        var vPicId = $(this).attr('data-pic-id');

        $.post("<%= ItemAdminRoutes.EDIT_IMAGE_SET_MAIN%>",
                {id: vId, picId: vPicId},
                function (data) {
                    var opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Основное изображение изменено.", "Запрос выполнен", opts);
                });
        return false;
    }
    function savePrice() {
        var id = $("#item-id").val();
        var price = $("#price").val();
        var balanceInStock = $("#balanceInStock").val();
        var takeIntoAccountTheBalanceInStock = $("#takeIntoAccountTheBalanceInStock").prop("checked");
        var available = $("#available").prop("checked");

        var req ={id: id, price: price, balanceInStock: balanceInStock,
            takeIntoAccountTheBalanceInStock: takeIntoAccountTheBalanceInStock, available: available};
        var saleId = $("#parent-sale-hidden").val();
        if (saleId == '')saleId='null';
        var req ={id: id, price: price, balanceInStock: balanceInStock,
            takeIntoAccountTheBalanceInStock: takeIntoAccountTheBalanceInStock, available: available, saleId:saleId};
        $.post("<%= ItemAdminRoutes.EDIT_PRICE%>",
                req,
                function (data) {
                    var opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Данные о цене изменены", "Запрос выполнен", opts);
                });
        return false;
    }
    function saveInfo() {
        var id = $("#item-id").val();
        var article = $("#article").val();
        var url = $("#url").val();
        var isTop = $("#isTop").prop("checked");
        var title = $("#title").val();
        var description = $("#description").val();
        var isNew = $("#isNew").prop("checked");


        var req = {id: id, article: article, url: url, isTop: isTop, title: title, description: description, isNew: isNew};
        var catalogId = $("#parent-hidden").val();
        if (catalogId != '')req['catalogId'] = catalogId;
        var brandId = $("#parent-brand-hidden").val();
        if (brandId != '')req['brandId'] = brandId;

        $.post("<%= ItemAdminRoutes.EDIT_INFO%>",
                req
                ,
                function (data) {
                    var opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Данные о товаре изменены.", "Запрос выполнен", opts);
                });
        return false;
    }
    $(function () {
        $('.item-set-main-image').click(setMainPic);
        $('.item-remove-image').click(removePic);
        $('#item-save-price').click(savePrice);
        $('#item-save-info').click(saveInfo);
    });
</script>