<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.pages.routes.PagesAdminRoutes" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ page import="com.ub.shop.catalog.routes.CatalogAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- Category-->
<div class="modal fade custom-width" id="modal-category">
    <div class="modal-dialog" style="width: 96%">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбор категории</h4>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-lg-5">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="query" name="query"
                                   value="" placeholder="Поиск"/>

                            <div class="input-group-btn">
                                <button type="submit" id="modal-category-search" class="btn btn-sm btn-default"><i class="entypo-search">Поиск </i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <section id="modal-catalog-parent-content"></section>


            </div>

            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<script>
    function initModal() {
        $.get("<%= CatalogAdminRoutes.MODAL_PARENT%>",
                { query: $('#query').val() },
                function (data) {
                    updateContent(data);
                });
    }

    function updateContent(data) {
        $('#modal-catalog-parent-content').html(data);
    }

    function onClickMTable() {
        var id = $(this).attr('data-id');
        var title = $(this).attr('data-title');
        $('#parent-hidden').val(id);
        $('#parent').val(title);
        $('#modal-category').modal('hide');
    }

    function onClickPaginator() {
        var q = $(this).attr('data-query');
        var n = $(this).attr('data-number');
        $.get("<%= CatalogAdminRoutes.MODAL_PARENT%>",
                { query: q , currentPage: n},
                function (data) {
                    updateContent(data);
                });
    }

    $(function () {
        $('#btn_parent_catalog').click(function () {
            $('#modal-category').modal('show');
            initModal();
            return false;
        });
        $('#btn_parent_catalog_clear').click(function () {
            $('#parent-hidden').val('');
            $('#parent').val('');
            return false;
        });

        $('#modal-category').on('click','.modal-catalog-line', onClickMTable);
        $('#modal-category').on('click','.modal-catalog-goto', onClickPaginator);
        $('#modal-category-search').click(initModal);

    });
</script>
