<%@ page import="com.ub.shop.order.routes.OrderAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<input type="hidden" id="order-id" value="${orderDoc.id}"/>
<div class="row">
    <div class="col-md-6">
        <ol class="breadcrumb bc-3">
            <li>
                <a href="<c:url value="/admin"/>"><i class="entypo-home"></i>Главная</a>
            </li>
            <li>
                <a href="<c:url value="<%= OrderAdminRoutes.ALL%>"/>">Все Заказы</a>
            </li>
            <li class="active">
                <strong>Редактирование</strong>
            </li>
        </ol>
    </div>
    <div class="col-md-6 text-right">
        <form action="<%= OrderAdminRoutes.ADD%>" method="POST">
            <button type="submit" class="btn btn-default">Добавить</button>
        </form>
    </div>
</div>
<c:if test="${currTab eq null}">
    <c:set var="currTab" value="profile-order"/>
</c:if>
<div class="row">
    <div class="col-md-12">
        <div class="panel minimal minimal-gray">
            <div class="panel-heading">
                <div class="panel-title"><h4>Редактирование заказа - ${orderDoc.payment.id}</h4></div>
                <div class="panel-options">
                    <ul class="nav nav-tabs">
                        <li <c:if test="${currTab eq 'profile-order'}">class="active"</c:if>>
                            <a href="#profile-order" data-toggle="tab">Общая информация о заказе</a>
                        </li>
                        <li <c:if test="${currTab eq 'profile-user'}">class="active"</c:if>>
                            <a href="#profile-user" data-toggle="tab">Информация о пользователе</a>
                        </li>
                        <li <c:if test="${currTab eq 'profile-delivery'}">class="active"</c:if>>
                            <a href="#profile-delivery" data-toggle="tab">Доставка</a>
                        </li>
                        <li <c:if test="${currTab eq 'profile-payment'}">class="active"</c:if>>
                            <a href="#profile-payment" data-toggle="tab">Оплата</a>
                        </li>
                        <li <c:if test="${currTab eq 'profile-items'}">class="active"</c:if>>
                            <a href="#profile-items" data-toggle="tab">Товары</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane <c:if test="${currTab eq 'profile-order'}">active</c:if>" id="profile-order">
                        <jsp:include page="editOrder.jsp"/>
                    </div>
                    <div class="tab-pane <c:if test="${currTab eq 'profile-user'}">active</c:if>" id="profile-user">
                        <jsp:include page="editUser.jsp"/>
                    </div>
                    <div class="tab-pane <c:if test="${currTab eq 'profile-delivery'}">active</c:if>" id="profile-delivery">
                        <jsp:include page="editDelivery.jsp"/>
                    </div>
                    <div class="tab-pane <c:if test="${currTab eq 'profile-payment'}">active</c:if>" id="profile-payment">
                        <jsp:include page="editPayment.jsp"/>
                    </div>
                    <div class="tab-pane <c:if test="${currTab eq 'profile-items'}">active</c:if>" id="profile-items">
                        <jsp:include page="editItems.jsp"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
