<%@ page import="com.ub.shop.item.routes.ItemAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<div class="row">
    <div class="col-md-6">
        <ol class="breadcrumb bc-3">
            <li>
                <a href="<c:url value="/admin"/>"><i class="entypo-home"></i>Главная</a>
            </li>
            <li class="active">
                <strong>Все товары</strong>
            </li>
        </ol>
    </div>
    <div class="col-md-6 text-right">
        <a href="<c:url value="<%= ItemAdminRoutes.ADD%>"/>" class="btn btn-default">Добавить</a>
    </div>
</div>
<div class="row">
    <form action="<%= ItemAdminRoutes.ALL%>" method="GET">
        <div class="col-lg-5">
            <div class="input-group">
                <input type="text" class="form-control input-sm" id="query" name="query"
                       value="${searchItemAdminResponse.query}" placeholder="Поиск"/>

                <div class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-default"><i class="entypo-search">Поиск </i></button>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="widget widget-blue">
    <div class="widget-title">
        <h3><i class="icon-table"></i> Результаты поиска: </h3>
    </div>
    <div class="widget-content">
        <div class="table-responsive">
            <table class="table table-bordered" id="table-1">
                <thead>
                <tr>

                    <th>Артикул</th>
                    <th>url</th>
                    <th>Заголовок</th>
                    <th>Цена со скидкой</th>
                    <th>Товар доступен</th>
                    <th>Остаток на складе</th>

                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${searchItemAdminResponse.result}" var="doc">

                    <tr>
                        <td>${doc.article}</td>
                        <td>${doc.url}</td>
                        <td>${doc.title}</td>
                        <td>${doc.discountPrice}</td>
                        <td>${doc.available}</td>
                        <td>${doc.balanceInStock}</td>

                        <td class="">
                            <c:url value="<%= ItemAdminRoutes.EDIT%>" var="editUrl">
                                <c:param name="id" value="${doc.id}"/>
                            </c:url>
                            <a href="${editUrl}" class="btn btn-default btn-xs">
                                <i class="icon-pencil">Редактировать</i>
                            </a>
                            <c:url value="<%= ItemAdminRoutes.DELETE%>" var="urlDelete">
                                <c:param name="id" value="${doc.id}"></c:param>
                            </c:url>
                            <a href="${urlDelete}" type="submit" class="btn btn-xs btn-danger  ">
                                <i class="icon-remove">Удалить</i>
                            </a>

                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 text-center">
        <ul class="pagination pagination-sm">
            <c:url value="<%= ItemAdminRoutes.ALL%>" var="urlPrev">
                <c:param name="query" value="${searchItemAdminResponse.query}"/>
                <c:param name="currentPage" value="${searchItemAdminResponse.prevNum()}"/>
            </c:url>
            <li><a href="${urlPrev}"><i class="entypo-left-open-mini"></i></a></li>

            <c:forEach items="${searchItemAdminResponse.paginator()}" var="page">
                <c:url value="<%= ItemAdminRoutes.ALL%>" var="urlPage">
                    <c:param name="query" value="${searchItemAdminResponse.query}"/>
                    <c:param name="currentPage" value="${page}"/>
                </c:url>
                <li class="<c:if test="${searchItemAdminResponse.currentPage eq page}">active</c:if>">
                    <a href="${urlPage}">${page + 1}</a>
                </li>
            </c:forEach>
            <c:url value="<%= ItemAdminRoutes.ALL%>" var="urlNext">
                <c:param name="query" value="${searchItemAdminResponse.query}"/>
                <c:param name="currentPage" value="${searchItemAdminResponse.nextNum()}"/>
            </c:url>
            <li><a href="${urlNext}"><i class="entypo-right-open-mini"></i></a></li>
        </ul>
    </div>
</div>
