<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.pages.routes.PagesAdminRoutes" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ page import="com.ub.shop.item.routes.ItemAdminRoutes" %>
<%@ page import="com.ub.shop.order.routes.OrderAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="row">
    <%--<div class="col-md-12">--%>
    <%--<button class="btn btn-xs btn-success">Добавить товар</button>--%>
    <%--</div>--%>
    <div class="col-lg-6">
        <form action="<%= OrderAdminRoutes.EDIT_ITEMS_ADD%>" method="POST">
            <input type="hidden" name="orderId" value="${orderDoc.id}"/>
            <label for="parent-item">Добавить товар</label>
            <input type="hidden" id="parent-item-hidden" name="itemId" class="form-control"
                   value="" required="required"/>
            <input type="text" class="form-control" id="parent-item"
                   value="" readonly="readonly"/>
            <a href="#" id="btn_parent_item" class="btn btn-blue btn-xs"
               style="margin-top: 10px">Выбрать</a>
            <button type="submit" id="order-add-item" class="btn btn-success btn-xs" style="margin-top: 10px">
                Добавить в заказ
            </button>
        </form>
    </div>

</div>
<br>
<br>

<c:set var="totalItemPrice" value="0"/>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered" id="items-table">
            <thead>
            <tr>
                <th>Артикул</th>
                <th>Наименование</th>
                <th>Цена</th>
                <th>Количество</th>
                <th>Сумма</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${orderDoc.items}" var="item">

                <tr>
                    <c:url value="<%=ItemAdminRoutes.EDIT%>" var="itemUrl">
                        <c:param name="id" value="${item.itemDoc.id}"/>
                    </c:url>
                    <td><a href="${itemUrl}">${item.itemDoc.article}</a></td>
                    <td><a href="${itemUrl}">${item.itemDoc.title}</a></td>
                    <td>${item.itemDoc.getEndPrice()}</td>
                    <td>${item.count}</td>
                    <td>${item.count * item.itemDoc.getEndPrice()}</td>
                    <c:set var="totalItemPrice" value="${totalItemPrice + item.count * item.itemDoc.getEndPrice()}"/>
                    <td class="">
                        <form action="<%= OrderAdminRoutes.EDIT_ITEMS_ADD%>" method="post" style="display: inline">
                            <input type="hidden" name="orderId" value="${orderDoc.id}"/>
                            <input type="hidden" name="itemId" value="${item.itemDoc.id}"/>
                            <button type="submit" class="btn btn-success btn-xs">
                                <i class="entypo-plus"></i>
                            </button>
                        </form>
                        <form action="<%= OrderAdminRoutes.EDIT_ITEMS_DELETE%>" method="post" style="display: inline">
                            <input type="hidden" name="orderId" value="${orderDoc.id}"/>
                            <input type="hidden" name="itemId" value="${item.itemDoc.id}"/>
                            <button type="submit" class="btn btn-xs btn-danger  ">
                                <i class="entypo-minus"></i>
                            </button>
                        </form>
                        <form action="<%= OrderAdminRoutes.EDIT_ITEMS_DELETE_ALL%>" method="post" style="display: inline">
                            <input type="hidden" name="orderId" value="${orderDoc.id}"/>
                            <input type="hidden" name="itemId" value="${item.itemDoc.id}"/>
                            <button type="submit" class="btn btn-xs btn-danger  ">
                                <i class="entypo-cancel"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <b>Общая сумма - </b>${totalItemPrice}
    </div>
</div>
