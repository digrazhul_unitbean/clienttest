<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form:errors path="*" cssClass="alert alert-warning" element="div" />
<form:hidden path="id"/>


<div class="row">
    <div class="col-lg-12">
        <label for="delivery">Данные о доставке</label>
        <form:input path="delivery" cssClass="form-control" id="delivery"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="payment">Оплата</label>
        <form:input path="payment" cssClass="form-control" id="payment"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="items">Товары</label>
        <form:input path="items" cssClass="form-control" id="items"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="status">Статус заказа</label>
        <form:input path="status" cssClass="form-control" id="status"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="userInfo">Данные пользователя</label>
        <form:input path="userInfo" cssClass="form-control" id="userInfo"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="createDate">Заказ создан</label>
        <form:input path="createDate" cssClass="form-control" id="createDate"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="lastUpdate">Последнее обновление заказа</label>
        <form:input path="lastUpdate" cssClass="form-control" id="lastUpdate"/>
    </div>
</div>

