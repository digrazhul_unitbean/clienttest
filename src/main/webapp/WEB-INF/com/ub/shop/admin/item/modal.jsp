<%@ page import="com.ub.shop.item.routes.ItemAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="modal fade custom-width" id="modal-item">
    <div class="modal-dialog" style="width: 96%">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбор товара</h4>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-lg-5">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="modal-item-query" name="query"
                                   value="" placeholder="Поиск"/>

                            <div class="input-group-btn">
                                <button type="submit" id="modal-item-search" class="btn btn-sm btn-default"><i
                                        class="entypo-search">Поиск </i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <section id="modal-item-parent-content"></section>


            </div>

            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<script>
    function initItemModal() {
        $.get("<%= ItemAdminRoutes.MODAL_PARENT%>",
                { query: $('#modal-item-query').val() },
                function (data) {
                    updateItemContent(data);
                });
    }

    function updateItemContent(data) {
        $('#modal-item-parent-content').html(data);
    }

    function onClickItemMTable() {
        var id = $(this).attr('data-id');
        var title = $(this).attr('data-title');
        $('#parent-item-hidden').val(id);
        $('#parent-item').val(title);
        $('#modal-item').modal('hide');
    }

    function onClickItemPaginator() {
        var q = $(this).attr('data-query');
        var n = $(this).attr('data-number');
        $.get("<%= ItemAdminRoutes.MODAL_PARENT%>",
                { query: q, currentPage: n},
                function (data) {
                    updateItemContent(data);
                });
    }

    $(function () {
        $('#btn_parent_item').click(function () {
            $('#modal-item').modal('show');
            initItemModal();
            return false;
        });
        $('#btn_parent_item_clear').click(function () {
            $('#parent-item-hidden').val('');
            $('#parent-item').val('');
            return false;
        });

        $('#modal-item').on('click', '.modal-item-line', onClickItemMTable);
        $('#modal-item').on('click', '.modal-item-goto', onClickItemPaginator);
        $('#modal-item-search').click(initItemModal);

    });
</script>
