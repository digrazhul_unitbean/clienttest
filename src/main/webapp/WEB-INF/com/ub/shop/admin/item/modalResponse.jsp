<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
    .curcor-pointer:hover {
        cursor: pointer;
        background-color: whitesmoke;
    }
</style>
<div class="row" style="margin-top: 10px">
    <div class="col-lg-12">
        <table class="table table-bordered" id="table-1">
            <thead>
            <tr>
                <th>Артикул</th>
                <th>Наименование</th>
                <th>Цена</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${searchItemAdminResponse.result}" var="doc">
                <tr class="curcor-pointer modal-item-line" data-id="${doc.id}" data-title="${doc.title}">
                    <td>${doc.article}</td>
                    <td>${doc.title}</td>
                    <td>${doc.getEndPrice()}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        <ul class="pagination pagination-sm">

            <li>
                <a href="#" class="modal-item-goto" data-query="${searchItemAdminResponse.query}"
                   data-number="${searchItemAdminResponse.prevNum()}">
                    <i class="entypo-left-open-mini"></i></a>
            </li>
            <c:forEach items="${searchItemAdminResponse.paginator()}" var="page">
                <li class="<c:if test="${searchItemAdminResponse.currentPage eq page}">active</c:if>">
                    <a href="#" class="modal-item-goto" data-query="${searchItemAdminResponse.query}"
                       data-number="${page}">${page + 1}</a>
                </li>
            </c:forEach>
            <li>
                <a href="#" class="modal-item-goto" data-query="${searchItemAdminResponse.query}"
                   data-number="${searchItemAdminResponse.nextNum()}"><i class="entypo-right-open-mini"></i></a>
            </li>
        </ul>
    </div>
</div>