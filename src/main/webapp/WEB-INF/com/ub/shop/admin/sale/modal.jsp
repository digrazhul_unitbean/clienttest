<%@ page import="com.ub.shop.sale.routes.SaleAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="modal fade custom-width" id="modal-sale">
    <div class="modal-dialog" style="width: 96%">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбор Распродажи(акции)</h4>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-lg-5">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="modal-sale-query" name="query"
                                   value="" placeholder="Поиск"/>

                            <div class="input-group-btn">
                                <button type="submit" id="modal-sale-search" class="btn btn-sm btn-default"><i
                                        class="entypo-search">Поиск </i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <section id="modal-sale-parent-content"></section>


            </div>

            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<script>
    function initSaleModal() {
        $.get("<%= SaleAdminRoutes.MODAL_PARENT%>",
                { query: $('#modal-sale-query').val() },
                function (data) {
                    updateSaleContent(data);
                });
    }

    function updateSaleContent(data) {
        $('#modal-sale-parent-content').html(data);
    }

    function onClickSaleMTable() {
        var id = $(this).attr('data-id');
        var title = $(this).attr('data-title');
        $('#parent-sale-hidden').val(id);
        $('#parent-sale').val(title);
        $('#modal-sale').modal('hide');
    }

    function onClickSalePaginator() {
        var q = $(this).attr('data-query');
        var n = $(this).attr('data-number');
        $.get("<%= SaleAdminRoutes.MODAL_PARENT%>",
                { query: q, currentPage: n},
                function (data) {
                    updateSaleContent(data);
                });
    }

    $(function () {
        $('#btn_parent_sale').click(function () {
            $('#modal-sale').modal('show');
            initSaleModal();
            return false;
        });
        $('#btn_parent_sale_clear').click(function () {
            $('#parent-sale-hidden').val('');
            $('#parent-sale').val('');
            return false;
        });

        $('#modal-sale').on('click', '.modal-sale-line', onClickSaleMTable);
        $('#modal-sale').on('click', '.modal-sale-goto', onClickSalePaginator);
        $('#modal-sale-search').click(initSaleModal);

    });
</script>
