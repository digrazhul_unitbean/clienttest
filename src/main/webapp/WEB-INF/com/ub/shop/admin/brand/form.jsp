<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form:errors path="*" cssClass="alert alert-warning" element="div"/>
<form:hidden path="id"/>


<div class="row">
    <div class="col-md-6">
        <label for="title">Название</label>
        <form:input path="title" cssClass="form-control" id="title"/>
    </div>

    <div class="col-md-6">
        <label for="picId">Картинка</label>
        <input type="file" class="form-control" id="picId" name="picFile"/>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label for="description">Описание</label>
        <form:textarea path="description" cssClass="form-control" id="description"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3>Текущая картинка:</h3>
        <p><img src="/pics/${brandDoc.picId}" style="width: 100%"/></p>
    </div>
</div>

