<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<div class="row">
    <div class="col-lg-6">
        <label for="title">Название</label>
        <input name="title" class="form-control" id="title" value="${catalogDoc.title}"/>
    </div>
    <div class="col-lg-6">
        <label for="pic">Картинка</label>
        <input type="file" class="form-control" id="pic" name="picFile"/>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-lg-12">
        <label for="url">Url</label>
        <input name="url" class="form-control" id="url" value="${catalogDoc.url }"/>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-lg-12">
        <label for="description">Описание</label>
        <textarea name="description" class="form-control" id="description" rows="6">${catalogDoc.description}</textarea>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-lg-12">
        <label for="parent">Родительский раздел</label>
        <input type="hidden" id="parent-hidden" name="parentId" class="form-control"
               value="${catalogDoc.parentId}"/>
        <input type="text" class="form-control" id="parent"
               value="${catalogDoc.getParentDoc().title}" readonly="readonly"/>
        <a href="#" id="btn_parent_catalog" class="btn btn-blue" style="margin-top: 10px">Выбрать</a>
        <a href="#" id="btn_parent_catalog_clear" class="btn" style="margin-top: 10px">Очистить</a>
    </div>

</div>

<hr/>

<div class="row">
    <div class="col-lg-12">
        <h3>Текущая картинка:</h3>

        <p><img src="/pics/${catalogDoc.picId}" style="width:100%"/></p>
    </div>
</div>