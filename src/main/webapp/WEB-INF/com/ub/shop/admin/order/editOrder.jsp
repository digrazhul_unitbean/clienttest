<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.pages.routes.PagesAdminRoutes" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ page import="com.ub.shop.order.models.OrderStatus" %>
<%@ page import="com.ub.shop.order.routes.OrderAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="row">
    <div class="col-lg-4">
        <select name="order-form-status" class="form-control" id="order-form-status"/>
        <c:forEach items="<%= OrderStatus.statuses%>" var="status">
            <option value="${status.id}" <c:if test="${status.id == orderDoc.status.id}">selected="selected"</c:if> >
                    ${status.title}
            </option>
        </c:forEach>
        </select>
    </div>
    <div class="col-lg-6">
        <button id="order-form-status-save" class="btn btn-success">Изменить статус заказа</button>
    </div>
</div>

<script>
    $('#order-form-status-save').click(function () {
        $.post("<%= OrderAdminRoutes.EDIT_STATUS%>",
                {
                    "id": $('#order-id').val(),
                    "status": $('#order-form-status option:selected').val()
                },
                function (data) {
                    var opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Статус заказа изменен", "Запрос выполнен", opts);
                });
    });
</script>