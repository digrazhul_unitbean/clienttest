<%@ page import="com.ub.shop.item.routes.ItemAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<div class="row">
    <div class="col-md-6">
        <ol class="breadcrumb bc-3">
            <li>
                <a href="<c:url value="/admin"/>"><i class="entypo-home"></i>Главная</a>
            </li>
            <li>
                <a href="<c:url value="<%= ItemAdminRoutes.ALL%>"/>">Все товары</a>
            </li>
            <li class="active">
                <strong>Добавление</strong>
            </li>
        </ol>
    </div>
    <div class="col-md-6 text-right">
        <a href="<c:url value="<%= ItemAdminRoutes.ADD%>"/>" class="btn btn-default">Добавить</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="widget widget-green" id="widget_profit_chart">
            <div class="widget-title">
                <h3><i class="icon-tasks"></i> Добавить</h3>
            </div>
            <div class="widget-content">

                <form:form action="<%= ItemAdminRoutes.ADD%>" modelAttribute="itemDoc" method="POST" enctype="multipart/form-data">

                    <c:set value="${itemDoc}" var="itemDoc" scope="request"/>
                    <jsp:include page="form.jsp"/>
                    <br>
                    <div class="form-group">
                        <button type="submit" id="contact-form-settings-submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form:form>

            </div>
        </div>
    </div>
</div>