<%@ page import="com.ub.shop.item.routes.ItemAdminRoutes" %>
<%@ page import="com.ub.shop.item.models.ItemDoc" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="row">
    <div class="col-md-6">
        <ol class="breadcrumb bc-3">
            <li>
                <a href="<c:url value="/admin"/>"><i class="entypo-home"></i>Главная</a>
            </li>
            <li>
                <a href="<c:url value="<%= ItemAdminRoutes.ALL%>"/>">Все товары</a>
            </li>
            <li class="active">
                <strong>Редактирование</strong>
            </li>
        </ol>
    </div>
    <div class="col-md-6 text-right">
        <a href="<c:url value="<%= ItemAdminRoutes.ADD%>"/>" class="btn btn-default">Добавить</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel minimal minimal-gray">

            <div class="panel-heading">
                <div class="panel-title"><h4>Редактирование товара - арт. <b>${itemDoc.article}</b></h4></div>
                <div class="panel-options">

                    <ul class="nav nav-tabs">
                        <c:forEach items="${tabsName}" var="tabPath">
                            <jsp:include page="${tabPath}"/>
                        </c:forEach>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="tab-content">
                    <c:forEach items="${tabs}" var="tabPath">
                        <jsp:include page="${tabPath}"/>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</div>
