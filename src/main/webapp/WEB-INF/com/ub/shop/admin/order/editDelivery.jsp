<%@ page import="com.ub.core.menu.models.fields.MenuFields" %>
<%@ page import="com.ub.core.pages.routes.PagesAdminRoutes" %>
<%@ page import="com.ub.core.file.FileRoutes" %>
<%@ page import="com.ub.shop.order.models.delivery.DeliveryStatus" %>
<%@ page import="com.ub.shop.order.models.delivery.DeliveryType" %>
<%@ page import="com.ub.shop.order.routes.OrderAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<section id="section-delivery">
    <div class="row">
        <div class="col-lg-4">
            <label for="delivery-form-status">Статус доставки</label>
            <select name="delivery-form-status" class="form-control" id="delivery-form-status"/>
            <c:forEach items="<%= DeliveryStatus.statuses%>" var="status">
                <option value="${status.id}" <c:if test="${status.id == orderDoc.delivery.status.id}">selected="selected"</c:if> >
                        ${status.title}
                </option>
            </c:forEach>
            </select>
        </div>
        <div class="col-lg-4">
            <label for="delivery-form-type">Тип доставки</label>
            <select name="delivery-form-type" class="form-control" id="delivery-form-type"/>
            <c:forEach items="<%= DeliveryType.types%>" var="dType">
                <option value="${dType.id}" <c:if test="${dType.id == orderDoc.delivery.type.id}">selected="selected"</c:if> >
                        ${dType.title}
                </option>
            </c:forEach>
            </select>
        </div>
        <div class="col-lg-4">
            <label for="delivery-form-price">Цена доставки</label>
            <input name="delivery-form-price" class="form-control" id="delivery-form-price"
                   value="${orderDoc.delivery.price}"/>
        </div>
    </div>
    <br>
    <hr>
    <br>

    <div class="row">
        <div class="col-lg-12">
            <h3>Куда доставить</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <label for="delivery-to-postcode">Почтовый индекс</label>
            <input name="delivery-to-postcode" class="form-control" id="delivery-to-postcode"
                   value="${orderDoc.delivery.to.postcode}"/>
        </div>
        <div class="col-lg-3">
            <label for="delivery-to-city">Город</label>
            <input name="delivery-to-city" class="form-control" id="delivery-to-city"
                   value="${orderDoc.delivery.to.city}"/>
        </div>
        <div class="col-lg-3">
            <label for="delivery-to-street">Улица</label>
            <input name="delivery-to-street" class="form-control" id="delivery-to-street"
                   value="${orderDoc.delivery.to.street}"/>
        </div>
        <div class="col-lg-2">
            <label for="delivery-to-house">Дом</label>
            <input name="delivery-to-house" class="form-control" id="delivery-to-house"
                   value="${orderDoc.delivery.to.house}"/>
        </div>
        <div class="col-lg-2">
            <label for="delivery-to-apartment">Квартира/офис</label>
            <input name="delivery-to-apartment" class="form-control" id="delivery-to-apartment"
                   value="${orderDoc.delivery.to.apartment}"/>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <label for="delivery-to-comment">Комментарий</label>
            <textarea name="delivery-to-comment" class="form-control"
                      id="delivery-to-comment">${orderDoc.delivery.to.comment}</textarea>
        </div>
    </div>

    <br>
    <hr>
    <br>

    <div class="row">
        <div class="col-lg-12">
            <h3>Откуда будет отправлена посылка</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <label for="delivery-from-postcode">Почтовый индекс</label>
            <input name="delivery-from-postcode" class="form-control" id="delivery-from-postcode"
                   value="${orderDoc.delivery.from.postcode}"/>
        </div>
        <div class="col-lg-3">
            <label for="delivery-from-city">Город</label>
            <input name="delivery-from-city" class="form-control" id="delivery-from-city"
                   value="${orderDoc.delivery.from.city}"/>
        </div>
        <div class="col-lg-3">
            <label for="delivery-from-street">Улица</label>
            <input name="delivery-from-street" class="form-control" id="delivery-from-street"
                   value="${orderDoc.delivery.from.street}"/>
        </div>
        <div class="col-lg-2">
            <label for="delivery-from-house">Дом</label>
            <input name="delivery-from-house" class="form-control" id="delivery-from-house"
                   value="${orderDoc.delivery.from.house}"/>
        </div>
        <div class="col-lg-2">
            <label for="delivery-from-apartment">Квартира/офис</label>
            <input name="delivery-from-apartment" class="form-control" id="delivery-from-apartment"
                   value="${orderDoc.delivery.from.apartment}"/>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <label for="delivery-from-comment">Комментарий</label>
            <textarea name="delivery-from-comment" class="form-control"
                      id="delivery-from-comment">${orderDoc.delivery.from.comment}</textarea>
        </div>
    </div>
    <br>
    <hr>
    <br>

    <div class="form-group">
        <button type="submit" id="delivery-save" class="btn btn-primary">
            Сохранить
        </button>
        <button type="submit" class="btn btn-default">
            Пересчитать стоимость
        </button>
    </div>
</section>

<script>
    $('#delivery-save').click(function () {
        var data = $('#section-delivery').serializeArray();
        $.post("<%= OrderAdminRoutes.EDIT_DELIVERY%>",
                {
                    "id": $('#order-id').val(),
                    "status.id": $('#delivery-form-status option:selected').val(),
                    "from.city": $('#delivery-from-city').val(),
                    "from.street": $('#delivery-from-street').val(),
                    "from.house": $('#delivery-from-house').val(),
                    "from.apartment": $('#delivery-from-apartment').val(),
                    "from.comment": $('#delivery-from-comment').val(),
                    "from.postcode": $('#delivery-from-postcode').val(),
                    "to.city": $('#delivery-to-city').val(),
                    "to.street": $('#delivery-to-street').val(),
                    "to.house": $('#delivery-to-house').val(),
                    "to.apartment": $('#delivery-to-apartment').val(),
                    "to.comment": $('#delivery-to-comment').val(),
                    "to.postcode": $('#delivery-to-postcode').val(),
                    "price": $('#delivery-form-price').val(),
                    "type.id": $('#delivery-form-type option:selected').val()
                },
                function (data) {
                    var opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Данные о доставке изменены", "Запрос выполнен", opts);
                });
    });
</script>