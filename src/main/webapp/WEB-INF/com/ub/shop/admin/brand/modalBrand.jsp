<%@ page import="com.ub.shop.brand.routes.BrandAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- Brand-->
<div class="modal fade custom-width" id="modal-brand">
    <div class="modal-dialog" style="width: 96%">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбор Бренда</h4>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-lg-5">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="modal-brand-query" name="query"
                                   value="" placeholder="Поиск"/>

                            <div class="input-group-btn">
                                <button type="submit" id="modal-brand-search" class="btn btn-sm btn-default"><i
                                        class="entypo-search">Поиск </i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <section id="modal-brand-parent-content"></section>


            </div>

            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<script>
    function initBrandModal() {
        $.get("<%= BrandAdminRoutes.MODAL_PARENT%>",
                { query: $('#modal-brand-query').val() },
                function (data) {
                    updateBrandContent(data);
                });
    }

    function updateBrandContent(data) {
        $('#modal-brand-parent-content').html(data);
    }

    function onClickBrandMTable() {
        var id = $(this).attr('data-id');
        var title = $(this).attr('data-title');
        $('#parent-brand-hidden').val(id);
        $('#parent-brand').val(title);
        $('#modal-brand').modal('hide');
    }

    function onClickBrandPaginator() {
        var q = $(this).attr('data-query');
        var n = $(this).attr('data-number');
        $.get("<%= BrandAdminRoutes.MODAL_PARENT%>",
                { query: q, currentPage: n},
                function (data) {
                    updateBrandContent(data);
                });
    }

    $(function () {
        $('#btn_parent_brand').click(function () {
            $('#modal-brand').modal('show');
            initBrandModal();
            return false;
        });
        $('#btn_parent_brand_clear').click(function () {
            $('#parent-brand-hidden').val('');
            $('#parent-brand').val('');
            return false;
        });

        $('#modal-brand').on('click', '.modal-brand-line', onClickBrandMTable);
        $('#modal-brand').on('click', '.modal-brand-goto', onClickBrandPaginator);
        $('#modal-brand-search').click(initBrandModal);

    });
</script>
