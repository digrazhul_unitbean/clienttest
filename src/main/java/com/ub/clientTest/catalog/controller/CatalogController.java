package com.ub.clientTest.catalog.controller;

import com.ub.clientTest.catalog.routes.CatalogRoutes;
import com.ub.shop.catalog.models.CatalogDoc;
import com.ub.shop.catalog.services.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by digrazhul on 15.05.2015.
 */

@Controller
public class CatalogController {

    @Autowired CatalogService catalogService;

    @RequestMapping(value = CatalogRoutes.ALL, method = RequestMethod.GET)
    public String all(Model model){
        List<CatalogDoc> parents = catalogService.getTopCatalog();
        Map<CatalogDoc, List<CatalogDoc>> childs = new HashMap<CatalogDoc, List<CatalogDoc>>();
        for (CatalogDoc parent : parents) {

            childs.put(parent, catalogService.getChildCatalogs(parent.getId()));

        }
        model.addAttribute("childs", childs);

        return "com.ub.client.catalog.all";
    }

    /*@RequestMapping(value = CatalogRoutes.VIEW, method = RequestMethod.GET)
    public String view(Model model){

        model.addAttribute("catalogDocs", catalogService.getChildCatalogs());

        return "com.ub.client.catalog.view";
    }*/

}
