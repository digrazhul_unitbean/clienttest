package com.ub.clientTest.catalog.routes;

/**
 * Created by digrazhul on 15.05.2015.
 */
public class CatalogRoutes {
    public static final String ALL = "/catalog";
    /*public static final String ALL = CATALOGS + "/{url}";*/
    public static final String VIEW = ALL + "/{url}";


    public static String VIEW(String url){
        return ALL + "/" + url;
    }
}
