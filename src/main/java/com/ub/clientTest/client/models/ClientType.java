package com.ub.clientTest.client.models;


public enum ClientType {
    Mr,Mrs;

    @Override
    public String toString(){
        if(this.equals(Mr)) return "Господин";
        if(this.equals(Mrs)) return "Госпожа";
        return "";
    }
}
