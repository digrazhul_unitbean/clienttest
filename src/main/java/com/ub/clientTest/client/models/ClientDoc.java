package com.ub.clientTest.client.models;

import com.ub.clientTest.clientType.models.ClientTypeDoc;
import org.bson.types.ObjectId;
import org.hibernate.validator.constraints.Email;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Size;


@Document
public class ClientDoc {

    private ObjectId id;
    @Email(message = "Введен не корректный email")
    private String email;
    @Size(min=3, max=40, message = "Имя должно быть длиной от 3 до 40 символов")
    private String name;
    private String lastName;
    private ObjectId pic;
    private ObjectId file;
    private ClientTypeDoc clientTypeDoc;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ObjectId getPic() {
        return pic;
    }

    public void setPic(ObjectId pic) {
        this.pic = pic;
    }

    public ObjectId getFile() {
        return file;
    }

    public void setFile(ObjectId file) {
        this.file = file;
    }

    public ClientTypeDoc getClientTypeDoc() {
        return clientTypeDoc;
    }

    public void setClientTypeDoc(ClientTypeDoc clientTypeDoc) {
        this.clientTypeDoc = clientTypeDoc;
    }
}
