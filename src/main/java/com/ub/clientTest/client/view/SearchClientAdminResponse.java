package com.ub.clientTest.client.view;

import com.ub.clientTest.client.models.ClientDoc;
import com.ub.core.base.search.SearchResponse;

import java.util.List;

/**
 * Created by digrazhul on 06.05.2015.
 */
public class SearchClientAdminResponse extends SearchResponse {
    private List<ClientDoc> result;

    public SearchClientAdminResponse(Integer currentPage, Integer pageSize, List<ClientDoc> result) {
        this.result = result;
        this.currentPage = currentPage;
        this.pageSize = pageSize;
    }

    public SearchClientAdminResponse(Integer currentPage, List<ClientDoc> result) {
        this.result = result;
        this.currentPage = currentPage;
    }

    public List<ClientDoc> getResult() {
        return result;
    }

    public void setResult(List<ClientDoc> result) {
        this.result = result;
    }

}
