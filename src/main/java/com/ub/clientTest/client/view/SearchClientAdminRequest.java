package com.ub.clientTest.client.view;

import com.ub.core.base.search.SearchRequest;

/**
 * Created by digrazhul on 06.05.2015.
 */
public class SearchClientAdminRequest extends SearchRequest {

    public SearchClientAdminRequest() {
    }

    public SearchClientAdminRequest(Integer currentPage) {
        this.currentPage = currentPage;
    }
}
