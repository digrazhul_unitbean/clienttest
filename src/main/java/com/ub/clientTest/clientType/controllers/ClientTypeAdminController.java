package com.ub.clientTest.clientType.controllers;

import com.ub.clientTest.clientType.models.ClientTypeDoc;
import com.ub.clientTest.clientType.routes.ClientTypeAdminRoutes;
import com.ub.clientTest.clientType.services.ClientTypeService;
import com.ub.clientTest.clientType.views.all.SearchClientTypeAdminRequest;
import com.ub.core.base.utils.RouteUtils;
import com.ub.core.base.views.breadcrumbs.BreadcrumbsLink;
import com.ub.core.base.views.pageHeader.PageHeader;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ClientTypeAdminController {
    @Autowired private ClientTypeService clientTypeService;

    private PageHeader defaultPageHeader(String current){
        PageHeader pageHeader = PageHeader.defaultPageHeader();
        pageHeader.setLinkAdd(ClientTypeAdminRoutes.ADD);
        pageHeader.getBreadcrumbs().getLinks().add(new BreadcrumbsLink(ClientTypeAdminRoutes.ALL,"Все Типы клиентов"));
        pageHeader.getBreadcrumbs().setCurrentPageTitle(current);
        return pageHeader;
    }

    @RequestMapping(value = ClientTypeAdminRoutes.ADD, method = RequestMethod.GET)
    public String create(Model model) {
        ClientTypeDoc clientTypeDoc = new ClientTypeDoc();
        clientTypeDoc.setId(new ObjectId());
        model.addAttribute("clientTypeDoc", clientTypeDoc);
        model.addAttribute("pageHeader",defaultPageHeader("Добавление"));
        return "com.ub.clientTest.admin.clientType.add";
    }

    @RequestMapping(value = ClientTypeAdminRoutes.ADD, method = RequestMethod.POST)
    public String create(@ModelAttribute ClientTypeDoc clientTypeDoc,
                         RedirectAttributes redirectAttributes) {
        clientTypeService.save(clientTypeDoc);
        redirectAttributes.addAttribute("id", clientTypeDoc.getId());
        return RouteUtils.redirectTo(ClientTypeAdminRoutes.EDIT);
    }

    @RequestMapping(value = ClientTypeAdminRoutes.EDIT, method = RequestMethod.GET)
    public String update(@RequestParam ObjectId id, Model model) {
        ClientTypeDoc clientTypeDoc = clientTypeService.findById(id);
        model.addAttribute("clientTypeDoc", clientTypeDoc);
        model.addAttribute("pageHeader",defaultPageHeader("Редактирование"));
        return "com.ub.clientTest.admin.clientType.edit";
    }

    @RequestMapping(value = ClientTypeAdminRoutes.EDIT, method = RequestMethod.POST)
    public String update(@ModelAttribute ClientTypeDoc clientTypeDoc,
                         RedirectAttributes redirectAttributes) {
        clientTypeService.save(clientTypeDoc);
        redirectAttributes.addAttribute("id", clientTypeDoc.getId());
        return RouteUtils.redirectTo(ClientTypeAdminRoutes.EDIT);
    }

    @RequestMapping(value = ClientTypeAdminRoutes.ALL, method = RequestMethod.GET)
    public String all(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                      @RequestParam(required = false, defaultValue = "") String query,
                      Model model) {
        SearchClientTypeAdminRequest searchClientTypeAdminRequest = new SearchClientTypeAdminRequest(currentPage);
        searchClientTypeAdminRequest.setQuery(query);
        model.addAttribute("searchClientTypeAdminResponse", clientTypeService.findAll(searchClientTypeAdminRequest));
        model.addAttribute("pageHeader",defaultPageHeader("Все"));
        return "com.ub.clientTest.admin.clientType.all";
    }

    @RequestMapping(value = ClientTypeAdminRoutes.MODAL_PARENT, method = RequestMethod.GET)
    public String modalResponse(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                @RequestParam(required = false, defaultValue = "") String query,
                                Model model) {
        SearchClientTypeAdminRequest searchClientTypeAdminRequest = new SearchClientTypeAdminRequest(currentPage);
        searchClientTypeAdminRequest.setQuery(query);
        model.addAttribute("searchClientTypeAdminResponse", clientTypeService.findAll(searchClientTypeAdminRequest));
        return "com.ub.clientTest.admin.clientType.modal.parent";
    }

    @RequestMapping(value = ClientTypeAdminRoutes.DELETE, method = RequestMethod.GET)
    public String delete(@RequestParam ObjectId id, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("pageHeader",defaultPageHeader("Удаление"));
        return "com.ub.clientTest.admin.clientType.delete";
    }

    @RequestMapping(value = ClientTypeAdminRoutes.DELETE, method = RequestMethod.POST)
    public String delete(@RequestParam ObjectId id) {
        clientTypeService.remove(id);
        return RouteUtils.redirectTo(ClientTypeAdminRoutes.ALL);
    }
}
