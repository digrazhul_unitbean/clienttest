package com.ub.clientTest.clientType.events;

import com.ub.clientTest.clientType.models.ClientTypeDoc;

public interface IClientTypeEvent {
    public void preSave(ClientTypeDoc clientTypeDoc);
    public void afterSave(ClientTypeDoc clientTypeDoc);

    public Boolean preDelete(ClientTypeDoc clientTypeDoc);
    public void afterDelete(ClientTypeDoc clientTypeDoc);
}
