package com.ub.clientTest.clientType.services;

import com.ub.clientTest.clientType.models.ClientTypeDoc;
import com.ub.clientTest.clientType.events.IClientTypeEvent;
import  com.ub.clientTest.clientType.views.all.SearchClientTypeAdminRequest;
import  com.ub.clientTest.clientType.views.all.SearchClientTypeAdminResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

@Component
public class ClientTypeService {
    private static Map<String,IClientTypeEvent> clientTypeEvents = new HashMap<String,IClientTypeEvent>();

    @Autowired private MongoTemplate mongoTemplate;

    public static void addClientTypeEvent(IClientTypeEvent iClientTypeEvent){
            clientTypeEvents.put(iClientTypeEvent.getClass().getCanonicalName(), iClientTypeEvent);
    }

    public ClientTypeDoc save(ClientTypeDoc clientTypeDoc){
            mongoTemplate.save(clientTypeDoc);
            callAfterSave(clientTypeDoc);
            return clientTypeDoc;
    }

    public ClientTypeDoc findById(ObjectId id){
            return mongoTemplate.findById(id, ClientTypeDoc.class);
    }

    public void remove(ObjectId id){
        ClientTypeDoc clientTypeDoc = findById(id);
        if (clientTypeDoc == null) return;
        mongoTemplate.remove(clientTypeDoc);
        callAfterDelete(clientTypeDoc);
    }

    public List<ClientTypeDoc> findAll(){
        return mongoTemplate.findAll(ClientTypeDoc.class);
    }

    public SearchClientTypeAdminResponse findAll(SearchClientTypeAdminRequest searchClientTypeAdminRequest) {
            Sort sort = new Sort(Sort.Direction.DESC, "id");
            Pageable pageable = new PageRequest(
                    searchClientTypeAdminRequest.getCurrentPage(),
                    searchClientTypeAdminRequest.getPageSize(),
                    sort);

            Criteria criteria = new Criteria();
            criteria = Criteria.where("title").regex(searchClientTypeAdminRequest.getQuery(), "i");

            Query query = new Query(criteria);
            Long count = mongoTemplate.count(query, ClientTypeDoc.class);
            query = query.with(pageable);

            List<ClientTypeDoc> result = mongoTemplate.find(query, ClientTypeDoc.class);
            SearchClientTypeAdminResponse searchClientTypeAdminResponse = new SearchClientTypeAdminResponse(
                    searchClientTypeAdminRequest.getCurrentPage(),
                    searchClientTypeAdminRequest.getPageSize(),
                    result);
            searchClientTypeAdminResponse.setAll(count.intValue());
            searchClientTypeAdminResponse.setQuery(searchClientTypeAdminRequest.getQuery());
            return searchClientTypeAdminResponse;
    }

    private void callAfterSave(ClientTypeDoc clientTypeDoc){
        for(IClientTypeEvent iClientTypeEvent : clientTypeEvents.values()){
            iClientTypeEvent.afterSave(clientTypeDoc);
        }
    }
    private void callAfterDelete(ClientTypeDoc clientTypeDoc){
        for(IClientTypeEvent iClientTypeEvent : clientTypeEvents.values()){
            iClientTypeEvent.afterDelete(clientTypeDoc);
        }
    }
}
